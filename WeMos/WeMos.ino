#include <ArduinoJson.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <WebSocketsClient.h>

#define MOV_SENSOR 16
#define RELAY_ONE 5
#define RELAY_TWO 4
#define RELAY_THREE 12
#define RELAY_FOUR 14
#define WS_PORT 81
#define WIRE_SDA 13
#define WIRE_SCL 2
#define SLAVE_ONE 8


boolean manualControl = false;

/**
 * Configuração do WI-FI
 */
//const char* ssid = "Bruno's Wi-Fi Network";
//const char* password = "ARDUINO2016";

const char* ssid = "VOID";
const char* password = "voidnowire";

WebSocketsClient webSocket;

void setup() {
  Serial.begin(9600);
  Wire.begin(WIRE_SDA,WIRE_SCL);
  //Ativar apenas modo estação
  WiFi.mode(WIFI_STA);
  //Iniciar o Modulo Wif-Fi
  WiFi.begin(ssid, password);
  //Aguarda até estar Ligado
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  //DEBUG
  Serial.println("");
  Serial.print("WiFi ligado a: ");
  Serial.println(ssid);

  /*
   * WebSocket connection
   */
  webSocket.begin("192.168.1.136", 8083,"/hardware");
  //Regista o Handler para tratar dos eventos do Socket
  webSocket.onEvent(webSocketEvent);
  //DEBUG
  Serial.println("WebSocket Iniciado");

  /* 
   * Imprime o endereço IP do Modulo
   */
  Serial.println(WiFi.localIP());
  pinMode(MOV_SENSOR,INPUT);
  pinMode(RELAY_ONE,OUTPUT);
  pinMode(RELAY_TWO,OUTPUT);
  pinMode(RELAY_THREE,OUTPUT);
  pinMode(RELAY_FOUR,OUTPUT);
  digitalWrite(RELAY_ONE,LOW);
  digitalWrite(RELAY_TWO,LOW);
  digitalWrite(RELAY_THREE,LOW);
  digitalWrite(RELAY_FOUR,LOW);

}

void loop() { 
    delay(500);
    bool movementDetected = false ;
    if(digitalRead(MOV_SENSOR) == 1){
      movementDetected = true;
      Serial.println("MOVEMENT DETECTED");
    }
    String payload="";
   
    Wire.requestFrom(SLAVE_ONE,32);
    int i = 0;
    while (Wire.available()){
      char c = Wire.read();
     if(c == 255){
      Serial.println(movementDetected);
         payload+= ","+String(movementDetected);
         webSocket.sendTXT(payload);
      break;
      }
      payload += c;
      i++;
    }
    webSocket.loop();
}

void controlLight(int lightNumber, int state){
  String stateStr = state == HIGH ? "ON" : "OFF";
  switch(lightNumber){
    case 1:
    //PNP
     digitalWrite(RELAY_ONE,state);
     Serial.println("LIGHT ONE "+stateStr);
     if(state == HIGH){
     manualControl = true;
     }
     break;
    case 2:
    //NPN
     digitalWrite(RELAY_TWO,state);
     Serial.println("LIGHT TWO "+stateStr);
     break;
    case 3:
    //NPN
     digitalWrite(RELAY_THREE,state);
     Serial.println("LIGHT THREE "+stateStr);
     break;
    case 4:
    //NPN
     digitalWrite(RELAY_FOUR,state == HIGH);
     Serial.println("LIGHT FOUR "+stateStr);
     break;
  }
}
 void json(String payload ){
       StaticJsonBuffer<300> jsonBuffer;
       JsonObject& root = jsonBuffer.parseObject(payload);
       if (!root.success()) {
        Serial.println("parseObject() failed");
        return;
        }
  boolean s1 = root["l1"];
  // Print values.
  Serial.println(s1);
}


/**
 * Processa os Eventos do Socket
 */
void webSocketEvent(WStype_t type, uint8_t * payload, size_t lenght) {
  switch(type) {
    case WStype_DISCONNECTED:
        //NOT USED
        break;
    case WStype_CONNECTED:
       //  webSocket.sendTXT("CAVE");
        break;
    case WStype_TEXT:
    //NOT USED
        break;
    case WStype_BIN:
    //não implementado neste exemplo
        break;
  }
}
