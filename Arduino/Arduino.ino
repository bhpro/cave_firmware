#include <Wire.h>
#define MOV_SENSOR 16
#define LIGHT_ONE A1
#define LIGHT_TWO A2
#define LIGHT_THREE A3
#define LIGHT_FOUR A6
#define ADDRESS 1
#define PHOTO_RESISTOR_PIN A7

String payload;
void setup() {
 Serial.begin(9600);
 Wire.begin(8);
 Wire.onRequest(sendData);
}

// Verifica se o sistema esta online
void isOnline(){
     int l1Voltage = 0;
     int l2Voltage = 0;
     int l3Voltage = 0;
     int l4Voltage = 0;
     l1Voltage = ((analogRead(LIGHT_ONE) * 5.0)/1024)*52;
     l2Voltage = ((analogRead(LIGHT_TWO) * 5.0)/1024)*52;
     l3Voltage = ((analogRead(LIGHT_THREE) * 5.0)/1024)*52;
     l4Voltage = ((analogRead(LIGHT_FOUR) * 5.0)/1024)*52;
     payload= String(String(l1Voltage)+","+String(l2Voltage)+","+String(l3Voltage)+","+String(l4Voltage)+","+String(processPhotoResistorData())+"\0");
}

 
void loop() {
  isOnline();
  //Processa Sensor
  processPhotoResistorData();
}


void sendData(){
  Wire.write(payload.c_str());
}

//LDR PROCESSING
double processPhotoResistorData(){
    int leitura_sensor = analogRead(PHOTO_RESISTOR_PIN); 
    return light(leitura_sensor);
}

//ADC TO LUX
double light (int RawADC0){
  double Vout=RawADC0*0.00322265625;//Leitura_ADC*(VOLTAGEM_REF*1024)
  int lux=(1650/Vout-500)/10;//(500*VOLTAGEM_REF/Vout-500)/RESISTOR
  return lux;
}
